package com.kata.sg.service.impl;

import static org.mockito.Matchers.longThat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kata.sg.domain.Account;
import com.kata.sg.repository.AccountRepository;
import com.kata.sg.repository.UserRepository;
import com.kata.sg.security.SecurityUtils;
import com.kata.sg.service.AccountService;
import com.kata.sg.service.UserService;
import com.kata.sg.service.dto.AccountDTO;
import com.kata.sg.service.mapper.AccountMapper;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private AccountMapper AccountMapper;
 
	@Autowired
	private UserRepository UserRepository;

	@Override
	public AccountDTO getAccountByIdUser() {
		
		Long userId = UserRepository.getUserIdByLogin(SecurityUtils.getCurrentUserByLogin());
		Account account = accountRepository.findOneByUserId(userId);
		AccountDTO accountDTO = AccountMapper.AccountTOAccountDTO(account);
		return accountDTO;
	}

	@Override
	public AccountDTO updateAccountByIdUser(Long amount) {
//
//		Long userId = UserRepository.getUserIdByLogin(SecurityUtils.getCurrentUserByLogin());
//		Account account = accountRepository.findOneByUserId(userId);
//		account.setAmount(amount);
//		AccountDTO accountDTO = AccountMapper.AccountTOAccountDTO(account);
//		return accountDTO;
		System.out.println("log amount "+amount);
		Long userId = UserRepository.getUserIdByLogin(SecurityUtils.getCurrentUserByLogin());
		Account account = accountRepository.findOneByUserId(userId);
		Long newValue = account.getAmount() - amount;
		account.setAmount(newValue);
		accountRepository.save(account);
		AccountDTO accountDTO = AccountMapper.AccountTOAccountDTO(account);
		return accountDTO;
	}

}
