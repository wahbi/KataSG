package com.kata.sg.service.dto;

public class AccountDTO {

	private Long id;

	private Long amount;
	
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

 
	
	
	

}
