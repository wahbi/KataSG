package com.kata.sg.service.mapper;

import java.util.Optional;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.kata.sg.domain.Account;
import com.kata.sg.service.dto.AccountDTO;

@Mapper(componentModel="spring")
public interface AccountMapper {
	
	
	Account AccountDTOTOAccount(AccountDTO accountDTO);
	

	AccountDTO AccountTOAccountDTO(Account account);

}
