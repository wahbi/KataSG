package com.kata.sg.service;

import java.util.Optional;

import com.kata.sg.service.dto.AccountDTO;

public interface AccountService {

	AccountDTO getAccountByIdUser();
	AccountDTO updateAccountByIdUser(Long amount);


}
