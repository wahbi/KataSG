/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kata.sg.web.rest.vm;
