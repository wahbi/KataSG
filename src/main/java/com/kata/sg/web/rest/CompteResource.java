package com.kata.sg.web.rest;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.kata.sg.service.AccountService;
import com.kata.sg.service.dto.AccountDTO;

import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class CompteResource {

	private final Logger log = LoggerFactory.getLogger(CompteResource.class);
	
	@Autowired
	private AccountService accountService;
	
	
    @GetMapping("/userCompte")
    @Timed
    public ResponseEntity<AccountDTO> getUserAccount() {
        log.debug("REST request to get getUserAccount : {}"); 
		return  new ResponseEntity<AccountDTO>(accountService.getAccountByIdUser(), HttpStatus.OK);
    }
    
    @PutMapping("/updateCompte")
    @Timed
    public ResponseEntity<AccountDTO> updateAccount(@RequestBody String amount) {
        log.debug("Request to update amount"); 
		return  new ResponseEntity<AccountDTO>(accountService.updateAccountByIdUser(Long.parseLong(amount)), HttpStatus.OK);
    }

}
