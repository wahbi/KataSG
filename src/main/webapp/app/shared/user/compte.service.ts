import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { ResponseWrapper } from '../model/response-wrapper.model';

 
import { Compte } from './compte.model';

@Injectable()
export class CompteService  {
    constructor(private http: Http) {


     }

    getCompte(): Observable<any> {
        return this.http.get(SERVER_API_URL + 'api/userCompte').map((res: Response) => res.json());
    }

    save(updateamount: number): Observable<any> {
        return this.http.post(SERVER_API_URL + 'api/updateCompte', updateamount);

    }

       update(amount: number): Observable<ResponseWrapper> {
        return this.http.put(SERVER_API_URL + 'api/updateCompte', amount)
            .map((res: Response) => this.convertResponse(res));



    }

      private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }
}
