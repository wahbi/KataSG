import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Account, LoginModalService, Principal ,Compte,CompteService} from '../shared';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.css'
    ] 

})
export class HomeComponent implements OnInit {
    account: Account;
    compte:Compte;
    modalRef: NgbModalRef;
    updateamount:any;
    operationFlag:boolean;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private CompteService: CompteService
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });

        this.getBalance();

        this.registerAuthenticationSuccess();
    }

    getBalance(){
        this.CompteService.getCompte().subscribe(data =>{
            this.compte = data;
        });
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
    save(){
        debugger;
        /*this.compte.amount = this.compte.amount - this.updateamount;*/
        this.CompteService.update(this.updateamount).subscribe(
            (response) => {
                if (response.status === 200) {
                    this.compte =response.json; 
                    this.operationFlag = true; 
                    
                } else {
                       this.operationFlag = false;
                }
            });
        this.updateamount = '';
      //  this.getBalance();
    }

/*    update(){
        this.CompteService.update(this.updateamount).subscribe(
            (response) => {
                if (response.status === 200) {
                    debugger;
                    
                } else {
                     debugger;
                }
            }); 
    }*/
        
}
